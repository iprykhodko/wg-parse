﻿
Parse.Cloud.job("POIupdate", function(request, status) {
    var counter = 0;
    var gpoi;
    // Query for all users
    var query = new Parse.Query("POI");
    query.skip(request.params.skip);
    query.limit(request.params.limit);
    query.find().then(function (results){
    // Create a trivial resolved promise as a base case.
    var promise = Parse.Promise.as();
    results.forEach(function(poi, index) {
      // For each item, extend the promise with a function
      promise = promise.then(function() {
        //address
        if (poi.get("Locality") != null) {
            return Parse.Cloud.run("getPOIAddress", {
                localityId: poi.get("Locality").id
            });
        } else {
            return "";
        }
      }).then(function(loc_str) {
        poi.set("LocalityStr",loc_str);
        //photos
        var query_photos = new Parse.Query("Photos");
        query_photos.equalTo("POI", poi);

        return query_photos.find();
    }).then(function(results_photos) {
          var photos_array = new Array();
          for (var k = 0; k < results_photos.length; ++k) {
              photos_array.push(results_photos[k]);
              console.log("k=" + k);
          }
          poi.set("PhotosList",photos_array);
          return;
    }).then(function() {
        if (counter % 10 === 0) {
            // Set the  job's progress status
            status.message(counter + " processed.");
        }
        counter += 1;
        console.log("counter=" + counter);
        return poi.save();
    });



    });

    return promise;
    }).then(function() {
        // Set the job's success status
        status.success("Job completed successfully.");
        //Parse.Cloud.run("getPoiList", {"limit":1});
    }, function(error) {
        // Set the job's error status
        status.error("Error: " + error.code + " " + error.message);
    });
});

Parse.Cloud.job("MonumentTypeId", function(request, status) {
    var counter = 0;

    var query = new Parse.Query("POI");
    query.each(function(poi) {
      // For each item, extend the promise with a function
      if (poi.get("MonumentType")!=null)
        poi.set("MonumentTypeId",poi.get("MonumentType").id);
      if (poi.get("Condition")!=null)
        poi.set("ConditionId",poi.get("Condition").id);
      if (poi.get("AuthorMonument")!=null)
        poi.set("AuthorMonumentId",poi.get("AuthorMonument").id);
      poi.save();
      counter++;
      if (counter % 10 === 0) {
            // Set the  job's progress status
            status.message(counter + " processed.");
        }
    }).then(function() {
        // Set the job's success status
        status.success("Job completed successfully.");
        //Parse.Cloud.run("getPoiList", {"limit":1});
    }, function(error) {
        // Set the job's error status
        status.error("Error: " + error.code + " " + error.message);
    });
});

Parse.Cloud.define("newUser", function(request, response) {
    Parse.Cloud.useMasterKey();
    var u_firstname="";
    var u_lastname="";
    var u_username="";
    var u_token="";
    var u_socialnet="";
    var u_avatar=null;
    var u_email="";
    var u_avatar_url=""

    if ("firstname" in request.params) {
        console.log("firstname");
        u_firstname=request.params.firstname;
    }
    if ("lastname" in request.params) {
        console.log("lastname");
        u_lastname=request.params.lastname;
    }
    if ("username" in request.params) {
        console.log("username");
        u_username=request.params.username;
    } else {
        response.error("Need: username");
    }
    if ("token" in request.params) {
        console.log("token");
        u_token=request.params.token;
    } else {
       response.error("Need: token");
    }
    if ("socialnet" in request.params) {
        console.log("socialnet");
        u_socialnet=request.params.socialnet;
    } else {
       response.error("Need: socialnet");
    }
    if ("avatar" in request.params) {
        console.log("avatar");
        u_avatar=request.params.avatar;
    }
    if ("email" in request.params) {
        console.log("email");
        u_email=request.params.email;
    }

    if ("avatar_url" in request.params) {
        console.log("avatar_url");
        u_avatar_url=request.params.avatar_url;
    }

    //check user
    var query = new Parse.Query("User");
    query.equalTo("username", u_username);
    query.equalTo("socialnet", u_socialnet);
    query.first({
      success: function(user) {
        if (user!=null) {
          console.log("user ok");
          user.set('password', u_token);
          if (u_firstname!="") user.set('firstname', u_firstname);
          if (u_lastname!="") user.set('lastname', u_lastname);
          if (u_email!="") user.set('email', u_email);
          if (u_avatar!="") user.set('avatar', u_avatar);
      
          user.save(null, {
              success: function(user) {
               response.success("Ok");
              },
              error: function(user, error) {
                response.error("Error: " + error.code + " " + error.message);
              }
            });

        } else {
          //new user
          var user = new Parse.User();
          user.set("username", u_username);
          user.set("password", u_token);
          user.set("socialnet", u_socialnet);

          if (u_firstname!="") user.set('firstname', u_firstname);
            //else {response.error("Need: firstname");return;}
          if (u_lastname!="") user.set('lastname', u_lastname);
            //else {response.error("Need: lastname"); return;}
          if (u_email!="") user.set('email', u_email);
            //else {response.error("Need: email"); return;}
          if (u_avatar!="") {user.set('avatar', u_avatar);}
            //load avatar
          Parse.Cloud.run("getImageFile", {
                  url: u_avatar_url
              }, {
                  success: function(result) {
                      if (result!=null) {
                        user.set('avatar', result);
                      }
                  },
                  error: function(error) {
                  }
          }).then(function(){
              user.signUp(null, {
                success: function(user) {
                  response.success(user._sessionToken);
                },
                error: function(user, error) {
                  response.error("Error: " + error.code + " " + error.message);
                }
              });
          });
          

        }
      },
      error: function(error) {
        response.error("Error: " + error.code + " " + error.message);
      }
    });



});


Parse.Cloud.define("getPoiList", function(request, response) {
    var res = new Array();
    var query1 = new Parse.Query("POI");
    var query2 = new Parse.Query("POI");

    //author
    if ("AuthorMonument" in request.params) {
        console.log("AuthorMonument");
        query1.equalTo("AuthorMonumentId", request.params.AuthorMonument);
        query2.equalTo("AuthorMonumentId", request.params.AuthorMonument);
    }

    //MonumentType
    if ("MonumentType" in request.params) {
        console.log("MonumentType");
        console.log(request.params.MonumentType);
        query1.containedIn("MonumentTypeId", request.params.MonumentType);
        query2.containedIn("MonumentTypeId", request.params.MonumentType);
    }

    //Near
    if ("near" in request.params) {
        console.log("near");
        query1.near("PlaceGPS", request.params.near);
        query2.near("PlaceGPS", request.params.near);
    }

	//Unset
    if ("unset" in request.params) {
        console.log("unset");
        query1.notEqualTo("objectId", request.params.unset);
        query2.notEqualTo("objectId", request.params.unset);
    }

    if (("southwestOfSF" in request.params) & ("northeastOfSF" in request.params)) {
        console.log("geo");
        query1.withinGeoBox("PlaceGPS", request.params.southwestOfSF, request.params.northeastOfSF);
        query2.withinGeoBox("PlaceGPS", request.params.southwestOfSF, request.params.northeastOfSF);
    } else {
        console.log("not geo");
    }

    if ("POI" in request.params) {
        console.log("get POI");
        query1.equalTo("objectId", request.params.POI);
        query2.equalTo("objectId", request.params.POI);
    } else {
        console.log("Get POI List");
    }


    var queryMain;

    //search
    if ("search" in request.params) {
        console.log("search");
        query1.contains("NameMonument", request.params.search);
        query2.contains("LocalityStr", request.params.search);
        queryMain = new Parse.Query.or(query1,query2);
    } else {
        queryMain = query1;
    }

    queryMain.skip(request.params.skip); // skip the first __ results
    queryMain.limit(request.params.limit); // limit to at most __ results
    queryMain.descending("views");

    //include data
    queryMain.include("PhotosList");
    queryMain.include("MonumentType");
    queryMain.include("Condition");
    queryMain.include("AuthorMonument");

    queryMain.find({
      success: function(results) {
        response.success(results);
      },
      error: function(error) {
        response.error("Error: " + error.code + " " + error.message);
      }
    });
});


Parse.Cloud.define("flowers", function(request, response) {
    //check flowers
    var user = request.user;
    var poiId = request.params.POI;

    if (!user) {
        response.error("Please login!");
    } else {
        //check count flowers
        console.log("check count flowers");
        //get POI Object
        var query = new Parse.Query("POI");
        query.equalTo("objectId", poiId);
        query.first().then(function(_POI) {
            return _POI;
        }).then(function(POI) {

            var query = new Parse.Query("Flowers");
            query.equalTo("User", user);
            query.equalTo("POI", POI);
            query.count({
                success: function(count) {
                    console.log("count=" + count);
                    if (count == 0) {
                        var Flowers = Parse.Object.extend("Flowers");
                        var new_flowers = new Flowers();
                        new_flowers.set("User", user);
                        new_flowers.set("POI", POI);
                        new_flowers.save(null, {
                            success: function(flowers) {
                                //incremennt count flowers
                                POI.increment("flowers");
                                POI.save().then(function (_POI){
                                  response.success(POI.get("flowers"));
                                });
                                console.log('New object created with objectId: ' + flowers.id);

                            },
                            error: function(flowers, error) {
                                console.log('Failed to create new object, with error code: ' + error.message);
                                response.error("error flower");
                            }
                        });


                    } else {
                        response.error("One flower per user");
                    }
                },
                error: function(error) {
                    console.log("Error: " + error.code + " " + error.message);
                    response.error("Error: " + error.code + " " + error.message);
                }
            });




        });


    }
});


Parse.Cloud.afterDelete("Photos", function(request) {
  var poi = request.object.get("POI");
  var query_photos = new Parse.Query("Photos");
  query_photos.equalTo("POI", poi);
  query_photos.find().then(function(results_photos) {
          var photos_array = new Array();
          for (var k = 0; k < results_photos.length; ++k) {
              photos_array.push(results_photos[k]);
              console.log("k=" + k);
          }
          poi.set("PhotosList",photos_array);
          poi.save();
    });
});

Parse.Cloud.afterSave("Photos", function(request) {
  var poi = request.object.get("POI");
  var query_photos = new Parse.Query("Photos");
  //query_photos.include("POI");
  query_photos.equalTo("POI", poi);
  query_photos.find().then(function(results_photos) {
          var photos_array = new Array();
          for (var k = 0; k < results_photos.length; ++k) {
              photos_array.push(results_photos[k]);
              console.log("k=" + k);
          }
          poi.set("PhotosList",photos_array);
          poi.save();
    });
});


Parse.Cloud.beforeSave("Photos", function(request, response) {
    var target_width = 640;
    var target_height = 480;

    var Image = require("parse-image");
    var photo = request.object;
    if (!photo.get("file")) {
        response.error("Users must have a profile photo.");
        return;
    }

    if (!photo.dirty("file")) {
        // The profile photo isn't being modified.
        response.success();
        return;
    }

    Parse.Cloud.httpRequest({
        url: photo.get("file").url()

    }).then(function(response) {
        var image = new Image();
        return image.setData(response.buffer);

    }).then(function(image) {
        // Resize the image to 640x480.
        var scale_ratio1 = image.width() / target_width;
        var scale_ratio2 = image.height() / target_height;
        var ratio = Math.min(scale_ratio1, scale_ratio2);

        return image.scale({
            ratio: 1 / ratio
        });

    }).then(function(image) {
        // Crop the image to the smaller of width or height.
        var width_border = (image.width() - target_width) / 2;
        var height_border = (image.height() - target_height) / 2;

        return image.crop({
            left: width_border,
            top: height_border,
            width: target_width,
            height: target_height
        });

    }).then(function(image) {
        // Make sure it's a JPEG to save disk space and bandwidth.
        return image.setFormat("JPEG");

    }).then(function(image) {
        // Get the image data in a Buffer.
        return image.data();

    }).then(function(buffer) {
        // Save the image into a new file.
        var base64 = buffer.toString("base64");
        var cropped = new Parse.File("thumbnail.jpg", {
            base64: base64
        });
        return cropped.save();

    }).then(function(cropped) {
        // Attach the image file to the original object.
        photo.set("thumbnail", cropped);

    }).then(function(result) {
        response.success();
    }, function(error) {
        response.error(error);
    });
});





Parse.Cloud.afterDelete("POI", function(request) {
  query = new Parse.Query("Photos");
  query.equalTo("POI", request.object);
  query.find({
    success: function(photos) {
      Parse.Object.destroyAll(photos, {
        success: function() {},
        error: function(error) {
          console.error("Error deleting related photos " + error.code + ": " + error.message);
        }
      });
    },
    error: function(error) {
      console.error("Error finding related photos " + error.code + ": " + error.message);
    }
  });
});


Parse.Cloud.afterDelete("_User", function(request) {
  query = new Parse.Query("POI");
  query.equalTo("AuthorMonument", request.object);
  query.find({
    success: function(poi_list) {
      Parse.Object.destroyAll(poi_list, {
        success: function() {},
        error: function(error) {
          console.error("Error deleting related poi " + error.code + ": " + error.message);
        }
      });
    },
    error: function(error) {
      console.error("Error finding related poi " + error.code + ": " + error.message);
    }
  });
});


Parse.Cloud.beforeSave("POI", function(request, response) {
  var poi = request.object;
  if (poi.dirty("MonumentType"))
    poi.set("MonumentTypeId", poi.get("MonumentType").id);
  if (poi.dirty("AuthorMonument"))
    poi.set("AuthorMonumentId", poi.get("AuthorMonument").id);
  if (poi.dirty("Condition"))
    poi.set("ConditionId", poi.get("Condition").id);
  response.success();
});


Parse.Cloud.afterSave("POI", function(request) {
  var poi = request.object;
  if (!poi.dirty("PlaceGPS")) {
    //get Locality String
    var placeGPS = poi.get("PlaceGPS");
    console.log("GPS ");
    var url_req='http://maps.googleapis.com/maps/api/geocode/json?language=RU&sensor=false&latlng='+ placeGPS.latitude +','+placeGPS.longitude;
    //console.log(url_req);
    Parse.Cloud.httpRequest({
      url: url_req,
      success: function(httpResponse) {
        var obj = JSON.parse(httpResponse.text);
        console.log("Status=" + obj.status);
        if (obj.status=="OK") {
          var country=null;
          var locality=null;
          var address_components = obj.results[0].address_components;
          address_components.forEach(function(component) {
            //console.log(component);
            if (component.types[0]=="country") country=component.long_name;
            if (component.types[0]=="locality") locality=component.short_name;
          });

          if (country!=null) {
           var address = country;
           if (locality!=null)
            address+=", " + locality;
           console.log(address);
           poi.set("LocalityStr", address);
           poi.save();
          }
        }
      },
      error: function(httpResponse) {
        console.error('Request failed with response code ' + httpResponse.status);
      }
    })
  }
});



/*
Parse.Cloud.afterSave("POI", function(request) {
  var poi = request.object;
  if (poi.get("Locality")!=null) {
    Parse.Cloud.run("getPOIAddress", {
      localityId: poi.get("Locality").id
      }).then(function (loc_str) {
        poi.set("LocalityStr", loc_str);
        poi.save();
      });
  }
});
*/

/*
Parse.Cloud.beforeSave("POI", function(request, response) {
  var poi = request.object;
  if (poi.get("Locality")!=null) {
    Parse.Cloud.run("getPOIAddress", {
      localityId: poi.get("Locality").id
      }).then(function (loc_str) {
        poi.set("LocalityStr", loc_str);
        response.success();
      });
  } else {
    response.success();
  }
});

*/

Parse.Cloud.define("getPOIAddress", function(request, response) {
    var locality;
    var area1;
    var country;
    var query = new Parse.Query("Locality");
    query.equalTo("objectId", request.params.localityId);
    query.first().then(function(_locality) {
        locality = _locality;
        //Country
        var query = new Parse.Query("Country");
        query.equalTo("objectId", locality.get("Country").id);
        return query.first();
    }).then(function(_country) {
        country = _country;
        var address = country.get("CountryName") + ", " + locality.get("LocalityName");
        console.log("adr=" + address);
        response.success(address);
    });
});


Parse.Cloud.define("setPoiViewed", function(request, response) {
    //get POI Object
    var query = new Parse.Query("POI");
    query.equalTo("objectId", request.params.POI);
    query.first({
        success: function(POI) {
            POI.increment("views");
            POI.save().then(function(_POI) {
                response.success(_POI.get("views"));
            });
        },
        error: function(error) {
            response.error(error.message);

        }
    });
});


Parse.Cloud.define("getPoi", function(request, response) {
    Parse.Cloud.run("getPoiList", {
        POI: request.params.POI
    }, {
        success: function(result) {
            //get POI Object
            var POI = result[0];
            //increment view POI
            POI.increment("views");
            POI.save().then(function(_POI) {
                response.success(result);
            });
        },
        error: function(error) {
            response.error(error);
        }
    });
});


Parse.Cloud.define("getImageFile", function (request, response) {
  if (request.params.url!="") {
    var Image = require("parse-image");
    Parse.Cloud.httpRequest({
        url: request.params.url

    }).then(function(response) {
        var image = new Image();
        return image.setData(response.buffer);

    }).then(function(image) {
        // Make sure it's a JPEG to save disk space and bandwidth.
        return image.setFormat("JPEG");

    }).then(function(image) {
        // Get the image data in a Buffer.
        return image.data();

    }).then(function(buffer) {
        // Save the image into a new file.
        var base64 = buffer.toString("base64");
        var avatar_img = new Parse.File("avatar.jpg", {
            base64: base64
        });
        return avatar_img.save();

    }).then(function(result) {
        response.success(result);
    }, function(error) {
        response.error(error);
    });




  }  else {
      response.error("url");
  }   

});


//add text